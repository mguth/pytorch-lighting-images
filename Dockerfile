FROM pytorch/pytorch:1.13.0-cuda11.6-cudnn8-runtime

## ensure locale is set during build
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y git h5utils wget curl

COPY requirements.txt .

RUN python -m pip install -r requirements.txt


